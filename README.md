# VR-Rides Framework 
**C#**, **Version 0.1**  
This is a software framework for immersive virtual reality application developer  
1. Use Google Street View as virtual scene easily.  
2. Use Oculus Rift or HTC Vive with Unity easily.  

## Getting Started 
Our environment: Windows x64 + [Unity 5.6.0f3](https://unity3d.com/get-unity/download/archive?_ga=2.221795032.175710595.1506322882-1200195442.1499989125)

### Prerequisites
- Install [MongoDB version 3.0](https://www.mongodb.com/download-center?filter=enterprise?jmp=nav#enterprise)  
- Install [Apache-Maven version 3.3.3](https://maven.apache.org/download.cgi#)  
- Install [java SE development Kit 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)  
- Go to *"Windows Properties\Advanced system settings\Environment Variables\System variables"*  
	**[if you use a different version, make sure you use the right path]**
	- Add new Variable `JAVA_HOME`, with value *"C:\Program Files\Java\jdk1.8.0_111"*  
- Open *"System variables\Path"*  
	**[if you use a different version, make sure you use the right path]**  
	- Add new value *"C:\Program Files\MongoDB\Server\3.0\bin"*  
	- Add new value *"C:\Program Files\apache-maven-3.3.3-bin\apache-maven-3.3.3\bin"*  
	- Add new value *"C:\Program Files\Java\jdk1.8.0_111\bin"*  

>__Note:__ Make sure all the values are added correctly

### Installing
```bash
# Fork and Clone this repository
> git clone https://<your_username>@bitbucket.org/positivecomputing/vr-rides-framework.git
```
- Cut the *"panorama-server"* folder in this repo, and paste it into your *"C:\Users\User Name\Documents\"* folder.  
- Cut the *".m2"* folder in this repo, and paste it into your *"C:\Users\User Name\"* folder.
- Create new empty folder with this path - *"C:\data\db"*.  

## Use with Unity 
### Before run Unity 
```bash
# Run Server through command line
# Open Command Prompt and run the following command
> mongod
# Showing "waiting for connections on port 27017" means correct

# Open another Command Prompt and run the following
> cd Documents
> cd panorama-server
> mvn tomcat7:run
# Showing "INFO: Starting ProtocolHandler ["http-bio-8081"]" means correct
```
## In Unity 
### Use Streetview as game scene / visual content 
1. Copy *"Assets"* folder into your Unity project.
2. In Unity, open *"Assets\VRStreetView\Resources\Prefabs"*.
3. Drag `HUD.prefab`, `LinkMarker.prefab` and `MapController.prefab` into your Unity scene.
4. In Hierachy, add your Unity camera object to `HUD->SceneManager->Player` and `Links->LinkManager->Camera`.
5. Run your project, you can have Street view as game scene.

>__Note:__ A sample scene is in *"Assets/SampleScenes/VisualContent.unity"*.  
>__Note:__ You can customize locations through replacing or adding StreetView ID in *"Assets\VRStreetView\Location.XML"*.  
>__Note:__ If you have more than one StreetView ID in XML file, you can press `{` or `}` button to change the place in game.

#### Get StreetView ID of your own places 
a) Open google maps with your browser, choose the location you want and browse street view images.  
b) Have a look at your StreetView url, which looks like  

		https://www.google.com.au/maps/@-33.8551251,151.210048,3a,75y,359.1h,95.63t/data=!3m6!1e1!3m4!1spMN5THtJmZF7AHmaom5Yrw!2e0!7i13312!8i6656?hl=en  
c) Copy the ID between `!1s` and `!`, which is `pMN5THtJmZF7AHmaom5Yrw`, this is your StreetView ID.  

### Use VR in Unity
>__Note:__ This framework already has [Oculus Utilities for Unity](https://developer.oculus.com/downloads/package/oculus-utilities-for-unity-5/), [Oculus Platform SDK](https://developer.oculus.com/downloads/package/oculus-platform-sdk/) and [SteamVR Plugin](https://www.assetstore.unity3d.com/en/#!/content/32647) included for you to use Oculus Rift or HTC Vive. Importing is slow because of *OvrAvatar*.
 
- ### Oculus Rift 


- ### HTC Vive 



### Reference 
This framework uses [VRTK](https://github.com/thestonefox/VRTK) for VR devices. More information about VRTK can be found [here](https://vrtoolkit.readme.io).