﻿using UnityEngine;
using UnityEngine.UI ;
using System.Collections;

public class PedalManager : MonoBehaviour {

	public bool Loading = false ;
	public bool HasTarget = false;
    public bool enable = true;
    private float progressPerPedal = 0.3f; 
    private float progressDropPerTick = 0f;
    private float progressDropAccelleration = 0.00001f;


    private float ticksPerSecond = 100f ;
	
	private float progress = 0f ;
	
	public float Progress {
		get { return progress; }
	}
	
	

	private Image icon ;
	private Text text ;

	public bool canPedal = true ;

	public void Pedal( float value) {

		if (!HasTarget || Loading) {
			progress = 0;
			return;
		}

		progressDropPerTick = 0 ;
		if (value > 0) {
			progress = (progress + progressPerPedal);
		}

		clampProgress();
	}

    public void Pedal()
    {

        if (!HasTarget || Loading)
        {
            progress = 0;
            return;
        }

        progressDropPerTick = 0;
       
            progress = (progress + progressPerPedal);
       

        clampProgress();
    }



	
	// Use this for initialization
	void Start () {
		icon = transform.Find ("Icon").GetComponent<Image> ();
		text = transform.Find ("Text").GetComponent<Text> ();

		InvokeRepeating("tick", 0, 1/ticksPerSecond);
	}

	private void clampProgress() {

		if (progress < 0f) 
			progress = 0f;

		if (progress > 1f) 
			progress = 1f;
	}

	private void tick() {

		if (!HasTarget || Loading) {
			progress = 0;
			return;
		}

        progressDropPerTick = progressDropPerTick + progressDropAccelleration;
        progress = progress - progressDropPerTick;
        clampProgress ();
	}

	void Update() {
		paint ();
	}
	
	private void paint() {

		if (Loading || !HasTarget) {
			icon.enabled = false ;
			text.enabled = false ;
			return ;
		}

		if (progress == 0) {
			icon.enabled = false;
			text.enabled = true;
			text.text = "Start pedaling to move" ;
		} 

		if (progress > 0) {
			icon.enabled = true;
			icon.fillAmount = progress;

			text.enabled = true;
			text.text = "Keep pedaling..." ;
		}
	}
}
