﻿
public class PanoramaLink {

	public string panoramaId { get; set; }
	
	public double latitude { get; set; }
	public double longitude { get; set; }
	
	public double bearing { get; set; }
	public double distance { get; set; }

	public bool selected { get; set; }
	
	public ImageDetails imageDetails { get; set; }
}
