﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LinkMarkerManager : MonoBehaviour {

	private bool selected = false ;
	private bool loading = false ;
	private float progress = 0f ;
	private string text = "" ;

	private Image baseIcon;
	private Image loadingIcon ;
	private Image selectedIcon ;

	private Text textBox ;
	
	


	public bool Selected {
		get { return selected  ;}
		set { this.selected = value; redraw() ;}
	}

	public bool Loading {
		get { return loading ; }
		set { this.loading = value; redraw() ;}
	}

	public float Progress {
		get { return this.progress; }
		set { this.progress = value; redraw() ;}
	}

	public string Text {

		get { 
			return text ;
		} 

		set {
			text = value ;
			redraw() ;
		} 
	}

	private void redraw() {

		if (baseIcon == null)
			return;

		baseIcon.enabled = true;

		loadingIcon.enabled = loading;
		//loadingIcon.fillAmount = progress;

		selectedIcon.enabled = selected;

		textBox.text = text;
	}

	// Use this for initialization
	void Start () {

		baseIcon = transform.Find ("Icon").GetComponent<Image>();
		loadingIcon = transform.Find ("LoadingIcon").GetComponent<Image>();
		selectedIcon = transform.Find ("SelectedIcon").GetComponent<Image>();

		textBox = transform.Find ("Text").GetComponent<Text> ();

		loadingIcon.enabled = false;

		redraw ();
	}
	

}
