﻿using UnityEngine;
using UnityEngine.VR;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using LitJson;
using System.Xml;

public class SceneManager : MonoBehaviour 
{

	private static string[] Sides = {"left","right","front","back","bottom","top"} ;

	public int zoom = 4 ;
	public string server = "http://localhost:8081" ;

	public GameObject player ;

	private List<Scene> scenes ;
	private int sceneIndex = 0 ;

	private Skybox skybox ;
	//private CompassManager compassManager ;
	private LinkManager linkManager ;
	private LoadIndicatorManager loadingManager ;
	private PedalManager pedalManager ;
	//private NetworkManager networkManager;

	private Dictionary<string,string> requestHeaders ;
    
	private Panorama currentPanorama ;
	//private Observer observer;
    private float yawDiff;

    private Texture2D texture;

    private Camera camera;
    private MapNav map;
    private int pointerFlag;

    // Use this for initialization
    void Start () 
	{
        scenes = new List<Scene>();

        //Read locations in XML file
        XmlDocument doc = new XmlDocument(); ;
        doc.Load(Application.dataPath + "\\VRStreetView\\Location.XML");
        XmlNodeList idList = doc.SelectNodes("/Location/ID");
        if (idList != null)
        {
            foreach (XmlNode id in idList)
            {
                //string panoID = id.InnerText.Trim();
                scenes.Add(new Scene(id.InnerText.ToString()));
            }
        }

        //gameManager = new GameManager();
        map = GameObject.Find("Map").GetComponent<MapNav>();

        //gameManager.addScene(scenes);
        
        //scenes.Add(new Scene("nmKY-AjI8huexasLGHbIJw"));
        //scenes.Add(new Scene("sdLxI1DRj3IZKUjeTDfBWg"));
        //scenes.Add(new Scene("XJZjrs4OCpHamCx2QWTYAg"));

        GameObject hud = GameObject.Find ("HUD");
		//compassManager = hud.transform.Find ("Compass").GetComponent<CompassManager> ();
		linkManager = hud.transform.Find ("Links").GetComponent<LinkManager> ();
		loadingManager = hud.transform.Find ("LoadIndicator").GetComponent<LoadIndicatorManager> ();
		pedalManager = hud.transform.Find ("PedalIndicator").GetComponent<PedalManager> ();
		//networkManager = GameObject.Find("Player").GetComponent<NetworkManager>();
		//observer = GameObject.Find("Observer").GetComponent<Observer>();
                

		camera = player.GetComponentInChildren<Camera> ();

		skybox = camera.GetComponent<Skybox>();
		if (skybox == null)
			skybox = camera.gameObject.AddComponent<Skybox>();


		requestHeaders = new Dictionary<string,string> ();
		requestHeaders.Add("Content-Type", "application/json");

		loadScene ();

        //headingFlag = true;
        pointerFlag = 0;
	}

	private void loadNextScene() {

		if (sceneIndex < scenes.Count - 1)
			sceneIndex ++;
		else
			sceneIndex = 0 ;

		loadScene ();
	}

	private void loadPreviousScene() {

		if (sceneIndex > 0)
			sceneIndex --;
		else
			sceneIndex = scenes.Count - 1 ;

		loadScene ();
	}
		

	private void loadScene() {

		//Debug.Log ("Loading scene " + sceneIndex);

		Scene scene = scenes [sceneIndex];

		StartCoroutine(loadPanorama (scene.getCurrentPanorama()));
	}

	private IEnumerator loadPanorama(string id) {

		//Debug.Log ("Loading panorama " + id);

		WWW panoRequest = new WWW (server + "/panoramas/" + id + "?zoom=" + zoom);
		yield return panoRequest;

		if (panoRequest.error != null) {
			Debug.LogError (panoRequest.error);
		} else {

			Panorama panorama = JsonMapper.ToObject<Panorama> (panoRequest.text);

            //GameManager.currentPano = panorama;
            //GlobalManager.currentPano = panorama;
            //GlobalManager.panorama = panorama;
            //tourGame.GetCurrentPano (panorama); //TourGame
            //GameGlobals.currentPano = panorama;

            //Debug.Log (panorama.id);

			scenes [sceneIndex].addPanorama (panorama.id);

			if (panorama.imageDetails.state == "ready")
				StartCoroutine (loadCachedPanorama (panorama));
			else 
				StartCoroutine (loadUncachedPanorama (panorama));
		}
	
	}

    private IEnumerator loadCachedPanorama(Panorama panorama)
    {

        //Vector3 MapRotation = GameObject.Find("Map").transform.eulerAngles;

        //Debug.Log("Loading chached panorama " + panorama.id);

        Material material = new Material(Shader.Find("Skybox/6 Sided"));

        loadingManager.Progress = 0f;
        loadingManager.IsLoading = true;
        pedalManager.Loading = true;

        //Debug.Log ("loading panorama images " + id); 

        foreach (string side in Sides)
        {

            // load the texture

            WWW imageRequest = new WWW(server + "/panoramas/" + panorama.id + "/images/" + side + ".jpg?zoom=" + zoom);
            yield return imageRequest;

            texture = imageRequest.texture;
            texture.wrapMode = TextureWrapMode.Clamp;

            material.SetTexture(getSkyboxSide(side), texture);

            yield return null;

            loadingManager.Progress = loadingManager.Progress + (1f / 6);
        }

        skybox.material = material;

        GameObject.Find("3D_Pointer").transform.rotation = Quaternion.Euler(new Vector3(0, (float)panorama.yaw, 0));

        if (currentPanorama != null)
        {
			//tourGame.GetCurrentPano (currentPanorama); //TourGame
            //preserve old player heading
            yawDiff = (float)(panorama.yaw - currentPanorama.yaw);
            //Debug.Log("yawDiff" + yawDiff);
            //Debug.Log ("currentPanorama.yaw" + currentPanorama.yaw);
            //Debug.Log("camera" + camera.transform.eulerAngles.y);
            //Debug.Log ("panorama.yaw" + panorama.yaw);

            //Debug.Log ("longitude: " + panorama.longitude);
            //Debug.Log ("latitude: " + panorama.latitude);

            //GlobalManager.panorama = currentPanorama;

            //Debug.Log("HEADING old: " + currentPanorama.yaw + " new: " + panorama.yaw + " diff: " + yawDiff);

            player.transform.Rotate(0, -yawDiff, 0);
            //Debug.Log("Y Rotation is: " + player.transform.rotation.eulerAngles.y);

            if ((int)Mathf.Abs(yawDiff) > 90)
                pointerFlag++;

            if (pointerFlag % 2 == 0)
            {
                GameObject.Find("3D_Pointer").transform.rotation = Quaternion.Euler(new Vector3(0, (float)panorama.yaw, 0));
            }
            else
                GameObject.Find("3D_Pointer").transform.rotation = Quaternion.Euler(new Vector3(0, 180+(float)panorama.yaw, 0));
        }


        

        //link manager
        linkManager.Clear();
        linkManager.SceneHeading = (float)panorama.yaw;
        linkManager.PlayerHeading = (float)player.transform.rotation.y;
        foreach (PanoramaLink link in panorama.links)
        {
            linkManager.AddLink(link);
        }

        loadingManager.Progress = 1f;
        loadingManager.IsLoading = false;
        pedalManager.Loading = false;

        //compass manager
        //compassManager.playerHeading = (float)player.transform.rotation.y;
        //compassManager.sceneHeading = (float)panorama.yaw;


        currentPanorama = panorama;
        //Debug.Log("Finished loading p " + panorama.id);
        //Debug.Log("Player y value is: " + player.transform.rotation.eulerAngles.y);
        //observer.GetValues(currentPanorama.latitude, currentPanorama.longitude, player.transform.rotation.eulerAngles.y);
        map.SetPosition((float)currentPanorama.latitude, (float)currentPanorama.longitude);

        //tourGame.ChangePlayerHeading (panorama.id);

    }
    

	private IEnumerator loadUncachedPanorama(Panorama panorama) {

		//Debug.Log ("Loading unchached panorama" + panorama.id);

		loadingManager.Progress = 0f ;
		loadingManager.IsLoading = true;
		pedalManager.Loading = true;


		ImageJob job = new ImageJob (panorama.id, zoom);
		
		string jobJson = JsonMapper.ToJson(job) ;
		byte[] jobBytes = Encoding.UTF8.GetBytes(jobJson.ToCharArray());
		
		//Debug.Log ("Posting job request " + job.ToString());
		
		WWW jobPost = new WWW(server + "/imagejobs/", jobBytes, requestHeaders);
		yield return jobPost;
		
		if (jobPost.error != null) {
			Debug.LogError(jobPost.error) ;
			yield break;
		}
		
		//Debug.Log (jobPost.text);
		
		job = JsonMapper.ToObject<ImageJob>(jobPost.text);
		
		//Debug.Log ("Received job response " + job.ToString());
		
		while (job.state != null && job.state == "running") {
			
			WWW jobGet = new WWW(server + "/imagejobs/" + job.id);
			yield return jobGet;
			
			if (jobGet.error != null) {
				Debug.LogError (jobGet.error);
				yield break;
			}

			job = JsonMapper.ToObject<ImageJob>(jobGet.text);
			
			//Debug.Log (" - " + job.ToString()) ;

			loadingManager.Progress = (float)job.progress ;

			/*
			if (job.state == "running") {
				link.imageDetails.progress = job.progress ;
				link.imageDetails.state = "loading" ;
			} else {
				link.imageDetails.progress = job.progress ;
				link.imageDetails.state = job.state ;
			}
			*/
		}

		loadingManager.Progress = 1f ;
		loadingManager.IsLoading = false;
		pedalManager.Loading = false;

		if (job.state == "completed") 
			StartCoroutine (loadCachedPanorama(panorama));
	}



    void Update()
    {
        /*
        if (!rightController)
        {
            rightController = GameObject.Find("Controller (right)");
        }
        if (!controllerTooltips)
            controllerTooltips = GameObject.Find("ControllerTooltips");
        if (!triggerTooltip)
            triggerTooltip = GameObject.Find("TriggerTooltip");
        if (rightController)
        {
            trackedObj = rightController.GetComponent<SteamVR_TrackedObject>();
            device = SteamVR_Controller.Input((int)trackedObj.index);
        }

        if (!cityCanvas.activeInHierarchy)
        {
            if (rightController && rightController.GetComponent<VRTK.VRTK_UIPointer>().enabled)
            {
                rightController.GetComponent<VRTK.VRTK_UIPointer>().enabled = false;
                rightController.GetComponent<VRTK.VRTK_SimplePointer>().enabled = false;
            }

            if (controllerTooltips)
                controllerTooltips.GetComponent<VRTK.VRTK_ControllerTooltips>().touchpadText = "Take a photo";

            if (triggerTooltip)
                triggerTooltip.SetActive(true);
        }
        else
        {
            if (rightController && !rightController.GetComponent<VRTK.VRTK_UIPointer>().enabled)
            {
                rightController.GetComponent<VRTK.VRTK_UIPointer>().enabled = true;
                rightController.GetComponent<VRTK.VRTK_SimplePointer>().enabled = true;
            }

            if (controllerTooltips)
                controllerTooltips.GetComponent<VRTK.VRTK_ControllerTooltips>().touchpadText = "Choose a city";

            if (triggerTooltip)
                triggerTooltip.SetActive(false);
        }
        */


        pedalManager.HasTarget = (linkManager.getSelectedLinkId() != null);
        /*
            if(pedalManager.HasTarget)
            {pedalManager.Pedal (networkManager.Speed);}
         * */

        // Open it to activate Sensors Data.

        //pedalManager.Pedal (networkManager.Speed);

        if (pedalManager.Progress >= 1) {
            if (linkManager.getSelectedLinkId() != null)
                StartCoroutine(loadPanorama(linkManager.getSelectedLinkId()));
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            pedalManager.enable = !pedalManager.enable;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)) {
            //Debug.Log ("UP - " + linkManager.getSelectedLinkId()) ;
            pedalManager.Pedal();
            if (pedalManager.Progress == 1) {
                if (linkManager.getSelectedLinkId() != null)
                    StartCoroutine(loadPanorama(linkManager.getSelectedLinkId()));
            }

        }

        if (Input.GetKeyDown(KeyCode.LeftBracket))
            loadPreviousScene();

        if (Input.GetKeyDown(KeyCode.RightBracket))
            loadNextScene();

        //if (!cityCanvas.activeInHierarchy)
        //{
            //if(rightController)
                //if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
                    //loadNextScene();
        //}

        if (Input.GetKeyDown(KeyCode.Space)) 
			InputTracking.Recenter() ;

		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();

        /*
        if (rightController)
            if(GameGlobals.SteamVRButtonPressable)
                if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
            //if (Input.GetKeyDown(KeyCode.J))
            {
                if (!cityCanvas.activeInHierarchy)
                    cityCanvas.SetActive(true);
                else
                    cityCanvas.SetActive(false);
            }

        if (GameGlobals.n_scene > 0) 
		{
			sceneIndex = GameGlobals.n_scene;
            GameGlobals.n_audio = GameGlobals.n_scene;
            loadScene ();
			GameGlobals.n_scene = -1;
			cityCanvas.SetActive (false);
		}
        */


        GameObject.Find("Map").transform.localPosition = new Vector3(0, 0, 0);
        GameObject.Find("3D_Pointer").transform.localPosition = new Vector3(0, 0.2f, 0);
    }

	private static string getSkyboxSide(string side) {
		
		switch (side) {
		case "front":
			return "_FrontTex";
		case "back":
			return "_BackTex";
		case "left":
			return "_RightTex";
		case "right":
			return "_LeftTex";
		case "top":
			return "_UpTex";
		case "bottom":
			return "_DownTex";
		}
		
		return null ;
	}
}
