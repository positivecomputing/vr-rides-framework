﻿public class Panorama {

	public string id { get; set; }
	
	public double latitude { get; set; }
	public double longitude { get; set; }
	
	public double elevation { get; set; }
	
	public double pitch { get; set; }
	public double yaw { get; set; }
	
	public string description { get; set; }
	
	public PanoramaLink[] links { get; set; }

	public ImageDetails imageDetails { get; set; }
}
