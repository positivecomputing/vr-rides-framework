﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic ;

public class Scene {

	private string initialPanoramaId ;

	private Stack<string> history ;
	
	public Scene(string panoramaId) {

		initialPanoramaId = panoramaId;
		history = new Stack<string> ();
	}

	public string getCurrentPanorama() {

		if (history.Count == 0)
			return initialPanoramaId;

		return history.Peek();
	}

	public void addPanorama(string id) {
		history.Push (id);
	}

	public string getPreviousPanorama() {
	
		if (history.Count > 0)
			history.Pop ();

		return getCurrentPanorama ();
	}



}
