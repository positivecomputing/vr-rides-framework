﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic ;
using UnityEngine.UI ;
using System;


public class LinkManager : MonoBehaviour {
	
	private float sceneHeading = 0f;
	
	public float SceneHeading {
		get { return sceneHeading; }
		set {
			sceneHeading = value;
			paint ();
		}
	}

	private float playerHeading = 0f;
	
	public float PlayerHeading {
		get { return playerHeading; }
		set {
			playerHeading = value;
			paint ();
		}
	}

	private float selectionThreshold = 0.3f ;
	public float SelectionThreshold {
		get { return selectionThreshold;}
		set {
			selectionThreshold = value;
			paint ();
		}
	}

	public Camera camera ;

	private Dictionary<string,PanoramaLink> linksById = new Dictionary<string,PanoramaLink>() ;
	private Dictionary<string,GameObject> linkMarkersById = new Dictionary<string,GameObject>() ;

	private string selectedLinkId ;

	public string getSelectedLinkId() {
		return selectedLinkId;
	}


	private class SelectionCandidate : IComparable<SelectionCandidate>{
		public string panoramaId ;
		public double distanceFromCenter ;

		public SelectionCandidate(string panoramaId, double distanceFromCenter) {
			this.panoramaId = panoramaId ;
			this.distanceFromCenter = distanceFromCenter ;
		}

		public int CompareTo(SelectionCandidate other)
		{
			return distanceFromCenter.CompareTo (other.distanceFromCenter);
		}
	}

	public void AddLink(PanoramaLink link) {

		//Debug.Log ("Added link to " + link.panoramaId);

		if (linksById.ContainsKey (link.panoramaId))
			return;
		
		linksById.Add (link.panoramaId, link);

		GameObject marker = Instantiate(Resources.Load("Prefabs/LinkMarker", typeof(GameObject))) as GameObject;
		marker.transform.parent = transform ;

		linkMarkersById.Add (link.panoramaId, marker);

		//marker.GetComponent<LinkMarkerManager>().Text = Mathf.RoundToInt((float)link.bearing).ToString() ;

		paint ();
	}

	public void RemoveLink(PanoramaLink link) {

		linksById.Remove (link.panoramaId);

		GameObject marker = linkMarkersById [link.panoramaId];
		linkMarkersById.Remove (link.panoramaId);

		if (marker != null) 
			Destroy(marker) ;

	}

	public void Clear() {

		foreach (GameObject marker in linkMarkersById.Values) {
			Destroy (marker) ;
		}

		linksById.Clear ();
		linkMarkersById.Clear() ;
		selectedLinkId = null;
	}


	

	// Use this for initialization
	void Start () {



	}

	private void select(string linkId) {

		//Debug.Log ("Selecting " + linkId);

		if (selectedLinkId != null && selectedLinkId != linkId) {

			GameObject selectedMarker = linkMarkersById [selectedLinkId];
			selectedMarker.GetComponent<LinkMarkerManager>().Selected = false ;
		}
	
		selectedLinkId = linkId;

		if (linkId != null) {
			GameObject linkMarker = linkMarkersById [linkId];
			linkMarker.GetComponent<LinkMarkerManager> ().Selected = true;
		}
	}

	private double updateMarker(PanoramaLink link, GameObject marker) {

		Vector3 worldPos = calculateWorldPosition (link);

		//Debug.Log (worldPos);

		Vector3 vpPos = camera.WorldToViewportPoint (worldPos);

		if (vpPos.z <= 0 ) {

			//this point is behind the camera, so hide it

			marker.SetActive(false) ;
			return -1 ;
		}

		//Debug.Log (vpPos);

		marker.transform.position = worldPos; //camera.WorldToScreenPoint(worldPos);
        //GlobalVariable.markerLocation = worldPos;
		marker.SetActive (true);

		double distanceFromCenter = Math.Sqrt (
			Math.Pow(vpPos.x - 0.5, 2) + Math.Pow(vpPos.y - 0.5, 2)
		);

		return distanceFromCenter ;
	}

	private Vector3 calculateWorldPosition(PanoramaLink link) {

		float heading = (float)(link.bearing - sceneHeading - playerHeading) ;

		//Debug.Log ("l:" + link.bearing + " s:" + sceneHeading + " p:" + playerHeading);
		//Debug.Log ("h:" + heading) ;

		Quaternion rotation = Quaternion.Euler(0, heading, 0);
		Vector3 forward = Vector3.forward * 300;

		return rotation * forward;
	}

	private void paint() {

		List<SelectionCandidate> selectionCandidates = new List<SelectionCandidate> ();

		foreach (PanoramaLink link in linksById.Values) {
			GameObject marker = linkMarkersById [link.panoramaId];
			double distanceFromCenter = updateMarker (link, marker);

			//Debug.Log (distanceFromCenter + " from center") ;

			if (distanceFromCenter > 0 && distanceFromCenter <= selectionThreshold) {
				//Debug.Log (link.panoramaId + " d:" + distanceFromCenter + " vs " + selectionThreshold) ;
				selectionCandidates.Add (new SelectionCandidate (link.panoramaId, distanceFromCenter));
			}
		}

		//Debug.Log (selectionCandidates.Count + " selection candidates");

		if (selectionCandidates.Count == 0) {
			select (null);
			return;
		}

		selectionCandidates.Sort ();
		select (selectionCandidates[0].panoramaId);

	}



	void OnGUI() {

		paint ();

	}
}
