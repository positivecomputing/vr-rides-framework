﻿using UnityEngine;
using UnityEngine.UI ;
using System.Collections;

public class LoadIndicatorManager : MonoBehaviour {


	private bool loading = false ;

	public bool IsLoading {
		get { return loading; }
		set {
			loading = value;
			paint ();
		}
	}

	private float progress = 0f ;

	public float Progress {
		get { return progress; }
		set {
			progress = value;
			paint ();
		}
	}


	
	private Image icon ;
	private Text text ;
	
	// Use this for initialization
	void Start () {
		icon = transform.Find ("Icon").GetComponent<Image> ();
		text = transform.Find ("Text").GetComponent<Text> ();

		paint ();
	}

	void paint() {

		if (!loading) {
			icon.enabled = false ;
			text.enabled = false ;
			return ;
		}

		icon.enabled = true ;
		text.enabled = true ;

		icon.fillAmount = progress;
	}

}
