﻿using UnityEngine;
using System.Collections;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using SimpleJSON;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;

public class NetworkManager : MonoBehaviour {
	bool running = false;
	private string direction;
	private long revolutions = -1;
	private float angle;
	private TcpClient client;
	private Thread receiveThread;
	private int port;
	private const string address = "127.0.0.1";
	private const string angleText = "a";
	private const int dataOffset = 6;
	private const string speedText ="s";
	private NetworkStream networkStream; 
	private int  delimiterPosition = 0;
    private PedalManager pedalManager;
	private int expectedBytes = 15;
	private long currentRevolutions = 0;


	// Use this for initialization
	void Start () {

		init();
	}

	private void init(){
		port = 9000;
		receiveThread = new Thread(new ThreadStart(receiveData));
		receiveThread.IsBackground = true;
		receiveThread.Start();
        GameObject hud = GameObject.Find("HUD");
        pedalManager = hud.transform.Find("PedalIndicator").GetComponent<PedalManager>();
	}


private void receiveData()
	 {
		try
		{
			
			client = new TcpClient(address,port);

			networkStream = client.GetStream();
			//networkStream.ReadTimeout = 100;
            while (client.Connected)
            {
				string data = "";
				if (networkStream.CanRead) {

					byte[] bytesReceived = new byte[client.ReceiveBufferSize];
					int bytesSize = networkStream.Read(bytesReceived,0,client.ReceiveBufferSize);

					if(bytesSize < expectedBytes)
					{
					data = Encoding.UTF8.GetString(bytesReceived);
					//Debug.Log("bytes received are:" + bytesSize);
						Debug.Log("data is"+ data);

					}
					
				}

				if(isValid(data)){
					
                    int startOfRevolutions = delimiterPosition + 1;
                    int endOfRotationAngle = delimiterPosition;
                    

                    long tempCurrentRevolutions = 0;
                    float tempAngle = 0.0f;
                    double tempSpeed = 0;

                    Debug.Log("Revolutions is " + data.Substring(startOfRevolutions,  1));
                    bool parseRevolutionSuccess = long.TryParse(data.Substring(startOfRevolutions, 1),out tempCurrentRevolutions);
				
					bool parseRotationAngleSuccess = float.TryParse(data.Substring(0,endOfRotationAngle),out tempAngle);

                   // bool parseSpeedSuccess = Double.TryParse(data.Substring(startSpeed, 4), out tempSpeed);

					if(parseRevolutionSuccess && parseRotationAngleSuccess)
					{
						this.Angle = tempAngle;
                        /*
                        if(tempCurrentRevolutions > currentRevolutions)
                        {
                            pedalManager.Pedal();
                            currentRevolutions = tempCurrentRevolutions;
                        }
                        */

                        currentRevolutions = tempCurrentRevolutions;
						if(currentRevolutions > 0)
						{
							pedalManager.Pedal();
                            currentRevolutions = 0;
						}
                    }
                }
			
			
			}

            networkStream.Close();
            client.Close();


			Debug.Log("All resources have been deallocated");
		}

		catch(Exception e){
			Debug.Log("All resources have been deallocated");
			networkStream.Close();
			client.Close();
			Debug.Log(e.Message);
		}

	}

	public float Angle
	{
		get{
			return angle;
		}

		set{
			angle = value;
		}
	}

    public long Revolutions
    {
        get
        {
            return this.currentRevolutions;
        }
    }



	// Update is called once per frame
	void Update () {
		/*
		if (!running) {
			StartCoroutine (retrieveValues());
		}
		*/
	}



	void OnApplicationQuit()
	{
		if (client == null) {
			Debug.Log("The client is null");
		}
		try{
			if(networkStream != null){
				networkStream.Close();
			}
			receiveThread.Join (500);
			//client.Close ();
			//receiveThread.IsBackground = false;

		}

		catch(Exception e)
		{

			Debug.Log(e.InnerException.Message);
		} 

		
	}

    /**
    This function if the incoming string data is valid. The string data should be in the form "angle,speed". If there is another delimiter after the speed then stop
        */
	private bool isValid(string values)
	{
        
		if (values == "") {
			return false;
		}
		
        int numberOfDelimiterOccurences = 0;

   
		for(int i = 0; i < values.Length;i++) {
            if(numberOfDelimiterOccurences > 1)
            {
                return false;
            }
			if(values[i].CompareTo(',') == 0)
			{
                numberOfDelimiterOccurences++;
                delimiterPosition = i;
                
			}
		}
        return true;
	}
    
}
