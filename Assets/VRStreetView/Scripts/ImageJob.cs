﻿using UnityEngine;
using System.Collections;

public class ImageJob {
	
	public string id ;

	public string panoramaId ;

	public int zoom ;

	public double progress ;

	public string state ;


	public ImageJob() {
		
	}

	public ImageJob(string panoramaId, int zoom) {
		this.panoramaId = panoramaId;
		this.zoom = zoom;
	}

	public string ToString() {

		string str = "";

		if (id != null)
			str = str + id  ;
		else
			str = "new";

		str = str + " pId: " + panoramaId ;

		if (state != null)
			str = str + " " + state;

		str = str + " progress: " + progress ;

		return str ;
	}
}
