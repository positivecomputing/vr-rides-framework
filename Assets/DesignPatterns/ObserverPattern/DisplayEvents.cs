﻿using UnityEngine;
using System.Collections;
using System;

namespace ObserverPattern
{
    public abstract class DisplayEvents
    {
        public abstract void DisplayData();
    }

    public class DisplayRealtimeScore : DisplayEvents
    {
        public override void DisplayData()
        {
            //add your own script here
            //set font, size, and the logic to display Realtime Score
        }
    }

    public class DisplayFinalScoreboard : DisplayEvents
    {
        public override void DisplayData()
        {
            //add your own script here
            //set font, size, and the logic to display Final Scoreboard
        }
    }
}