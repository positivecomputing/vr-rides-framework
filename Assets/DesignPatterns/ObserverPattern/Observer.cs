﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObserverPattern
{
    public abstract class Observer
    {
        public abstract void OnNotify();
    }

    public class Display : Observer
    {
        GameObject obj;
        DisplayEvents displayEvent;

        public Display(GameObject obj, DisplayEvents displayEvent)
        {
            this.obj = obj;
            this.displayEvent = displayEvent;
        }
        
        public override void OnNotify()
        {
            //add your own script here for display part
            //display();
        }
        
        //your own function for display
        //private void display()
        //{
        //}
    }
}
