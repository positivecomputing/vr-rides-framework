﻿using UnityEngine;
using System.Collections;

namespace ObserverPattern
{
    public class GameController : MonoBehaviour
    {
        public GameObject sphereObj;

        public GameObject obj_data1;
        public GameObject obj_data2;
        public GameObject obj_data3;
        
        Subject subject = new Subject();

        public static int new_data;
        private int old_data;

        void Start()
        {
            Display box1 = new Display(obj_data1, new JumpLittle());
            Display box2 = new Display(obj_data2, new JumpMedium());
            Display box3 = new Display(obj_data3, new JumpHigh());
            
            subject.AddObserver(box1);
            subject.AddObserver(box2);
            subject.AddObserver(box3);

            old_data = 0;
        }


        void Update()
        {
            if (old_data != new_data)
            {
                subject.Notify();
                old_data = new_data;
            }
        }
    }
}