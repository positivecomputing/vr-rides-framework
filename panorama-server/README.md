**Panorama Server** is a webservice that caches google streetview data (both imagery and structured data like coordinates). It is intended to be run locally on your own machine, to improve the performance of anything that relies on streetview. It also provides a nice, clean, well-documented API for retrieving this data. 

##Running Panorama Server

1.  Make sure you have [MongoDB](https://www.mongodb.org/) installed and running locally. 
 
2.  Ensure you are [authenticated with our maven repo](http://wiki.poscomp.org/mvn.poscomp.org) as either a *developer* or *collaborator*.

3.  Navigate to the project's web source code and install bower components

    ```
    cd src/main/webapp
    ```

    ```    
    bower install
    ```

4.  Navigate back to the project root, and run via maven

    ```
    cd -
    ```

    ```
    mvn tomcat7:run
    ```

You should now be able to navigate to [http://localhost:8081](http://localhost:8081) in your browser, to see nice documentation about how to use the panorama API. 

##Usage

The API is fully documented at [http://localhost:8081](http://localhost:8081).

All of these functions rely on you first knowing the ID of a starting panorama. Some nice staring points are:

* Great Barrier Reef: `N71STQcgRZEAAAQfCadA5A`
* El Capitan, Yosemite: `hE9PFK4uiU0AAAQqZe0Gfw`
* Mount Everest: `TLc2AM_nU88AAAQpjCtyxw`

To find a different starting point, the easiest way is via [Google Maps](https://www.google.com.au/maps). Just drag the streetview widget onto the location, then look at the url for the *data* query parameter. It should look something like

`data=!3m7!1e1!3m5!1s69_1P5_U1Oo48xr4XqSM1w!2e0!6s...`

This cryptic thing is delimited with the `!` character. Look for the first delimited string that starts with `1s`, in this case `1s69_1P5_U1Oo48xr4XqSM1w`. From that, you can get your panorama by dropping the `1s`. 

So, our panorama id is `69_1P5_U1Oo48xr4XqSM1w`.


##A word about data ownership

Bear in mind that this data belongs to Google, or the individuals who uploaded the imagery. Google's terms and conditions allow caching of this data for performance reasons, for up to 30 days. So, using this server in that context should be fine. But please don't abuse it to crawl large chunks of Google's data, attempt to archive or share that data with anyone, and definitely don't put this service up for the general public to use, where it could easily be abused.