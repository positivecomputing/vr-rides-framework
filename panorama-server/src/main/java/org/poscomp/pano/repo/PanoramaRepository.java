package org.poscomp.pano.repo;

import org.bson.types.ObjectId;
import org.poscomp.pano.model.Panorama;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by dnmilne on 13/07/15.
 */
@Repository
public class PanoramaRepository {

    @Autowired
    private MongoTemplate m;

    public Panorama findOne(String id) {
        return m.findById(id, Panorama.class) ;
    }

    public List<Panorama> findNear(double latitude, double longitude, double radius) {

        Query query = new Query()
                .addCriteria(
                        Criteria.where("lnglat")
                            .nearSphere(new Point(longitude, latitude))
                            .maxDistance(radius)
                ) ;

        return m.find(query, Panorama.class) ;
    }

    public Panorama save(Panorama panorama) {
        m.save(panorama) ;

        return panorama ;
    }



}
