package org.poscomp.pano.repo;

import org.poscomp.pano.model.Projection;
import org.poscomp.skybox.Side;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created by dnmilne on 13/07/15.
 */
@Repository
@PropertySource("classpath:image.properties")
public class ImageRepository implements InitializingBean {

    @Autowired
    private Environment env ;

    private File baseDir ;



    public boolean exists(String panoramaId, int zoomLevel, Projection projection) {


        File file = getFile(panoramaId, zoomLevel, projection) ;
        return file.exists() ;
    }

    public BufferedImage get(String panoramaId, int zoomLevel, Projection projection) throws IOException {

        File file = getFile(panoramaId, zoomLevel, projection) ;
        if (!file.exists())
            return null ;

        return ImageIO.read(file) ;
    }

    public byte[] getAsBytes(String panoramaId, int zoomLevel, Projection projection) throws IOException {

        File file = getFile(panoramaId, zoomLevel, projection) ;
        if (!file.exists())
            return null ;

        return Files.readAllBytes(file.toPath()) ;
    }



    public BufferedImage save(BufferedImage image, String panoramaId, int zoomLevel, Projection projection) throws IOException {

        File file = getFile(panoramaId, zoomLevel, projection) ;
        file.mkdirs() ;

        ImageIO.write(image, "jpg", file) ;

        return image ;
    }

    private File getFile(String panoramaId, int zoomLevel, Projection projection) {

        return new File(baseDir.getPath() + "/" + panoramaId + "/" + zoomLevel + "/" + projection + ".jpg") ;
    }


    public void afterPropertiesSet() throws Exception {
        String baseDirPath = env.getProperty("base.directory") ;

        baseDir = new File(baseDirPath) ;
        baseDir.mkdir() ;
    }


}
