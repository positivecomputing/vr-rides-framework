package org.poscomp.pano.repo;

import org.bson.types.ObjectId;
import org.poscomp.pano.model.ImageJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by dnmilne on 14/07/15.
 */
@Repository
public class ImageJobRepository {

    @Autowired
    private MongoTemplate m;

    public List<ImageJob> find(Date startedBefore, String panoramaId, ImageJob.State state) {

        Query query = new Query()
                .limit(20)
                .with(new Sort(Sort.Direction.DESC, "startedAt")) ;

        if (startedBefore != null)
            query.addCriteria(Criteria.where("startedAt").lt(startedBefore)) ;

        if (panoramaId != null)
            query.addCriteria(Criteria.where("panoramaId").is(panoramaId)) ;

        if (state != null)
            query.addCriteria(Criteria.where("state").is(state)) ;

        return m.find(query, ImageJob.class) ;
    }

    public ImageJob findOne(ObjectId id) {

        return m.findById(id, ImageJob.class) ;
    }

    public ImageJob findLatest(String panoramaId, int zoom) {

        Query query = new Query()
                .addCriteria(Criteria.where("panoramaId").is(panoramaId))
                .addCriteria(Criteria.where("zoom").is(zoom))
                .limit(1)
                .with(new Sort(Sort.Direction.DESC, "startedAt")) ;

        return m.findOne(query, ImageJob.class) ;
    }

    public ImageJob save(ImageJob job) {
        m.save(job) ;
        return job ;
    }


}
