package org.poscomp.pano.model;

import org.bson.types.ObjectId;
import org.poscomp.streetview4j.model.PanoramaDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by dnmilne on 21/10/15.
 */
public class CrawlJob {

    private static final Logger LOG = LoggerFactory.getLogger(CrawlJob.class) ;

    private Seed seed ;

    private int zoom ;
    private int maxSteps ;

    private int currentSteps = 0 ;

    private Map<String,Location> working = new LinkedHashMap<String,Location>() ;
    private Map<String,Location> aborted = new LinkedHashMap<String,Location>() ;
    private Map<String,Location> completed = new LinkedHashMap<String,Location>() ;

    private Queue<Item> pending = new LinkedList<Item>();


    private Date startedAt ;
    private Date completedAt ;

    private CrawlJob() {

    }

    public CrawlJob(Panorama pano, int zoom, int maxSteps) {

        this.seed = new Seed(pano) ;
        this.zoom = zoom ;
        this.maxSteps = maxSteps ;

        pending.offer(new Item(pano, 0)) ;

        this.startedAt = new Date() ;
    }

    public Seed getSeed() {
        return seed;
    }

    public int getZoom() {
        return zoom;
    }

    public int getMaxSteps() {
        return maxSteps;
    }

    public int getCurrentSteps() {
        return currentSteps;
    }

    public synchronized void offer(PanoramaDetails pano, int depth) {

        if (working.containsKey(pano.getLocation().getPanoId()))
            return ;

        if (completed.containsKey(pano.getLocation().getPanoId()))
            return ;

        if (aborted.containsKey(pano.getLocation().getPanoId()))
            return ;

        pending.offer(new Item(pano, depth)) ;
    }

    public synchronized Item poll() {

        while (!pending.isEmpty()) {

            Item item = pending.poll() ;

            if (working.containsKey(item.getPanoramaId()))
                continue ;

            if (completed.containsKey(item.getPanoramaId()))
                continue ;

            if (aborted.containsKey(item.getPanoramaId()))
                continue ;

            working.put(item.getPanoramaId(), new Location(item)) ;
            currentSteps = item.getSteps() ;

            return item ;
        }

        this.completedAt = new Date() ;
        return null ;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public Date getCompletedAt() {
        return completedAt;
    }

    public Map<String,Location> getCompleted() {
        return completed;
    }

    public Map<String,Location> getWorking() {
        return working;
    }

    public Map<String,Location> getAborted() {
        return aborted;
    }

    public Queue<Item> getPending() {
        return pending;
    }

    public synchronized void handleCompleted(String panoId) {

        Location location = working.get(panoId) ;

        if (location == null) {
            LOG.warn("Could not locate working pano " + panoId) ;
            return ;
        }

        completed.put(panoId, location) ;
        working.remove(panoId) ;
    }


    public synchronized void handleAborted(String panoId) {

        Location location = working.get(panoId) ;

        if (location == null) {
            LOG.warn("Could not locate working pano " + panoId) ;
            return ;
        }

        aborted.put(panoId, location) ;
        working.remove(panoId) ;
    }


    public static class Item {

        private String panoramaId ;
        private double latitude ;
        private double longitude ;

        private int steps ;

        private Item() {

        }

        public Item(PanoramaDetails pano, int steps) {

            this.panoramaId = pano.getLocation().getPanoId();
            this.latitude = pano.getLocation().getLat() ;
            this.longitude = pano.getLocation().getLng() ;

            this.steps = steps ;
        }

        public Item(Panorama pano, int depth) {

            this.panoramaId = pano.getId() ;
            this.latitude = pano.getLngLat()[1] ;
            this.longitude = pano.getLngLat()[0] ;
        }

        public String getPanoramaId() {
            return panoramaId;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public int getSteps() {
            return steps;
        }
    }

    public static class Location {

        private double latitude ;
        private double longitude ;

        private Location() {

        }

        public Location(Item item) {
            this.latitude = item.latitude ;
            this.longitude = item.longitude ;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

    }

    public static class Seed {

        private String id ;

        private double latitude ;
        private double longitude ;

        private Seed() {

        }

        public Seed(String id, double latitude, double longitude) {

            this.id = id ;
            this.latitude = latitude ;
            this.longitude = longitude ;
        }

        public Seed(Panorama pano) {
            this.id=pano.getId() ;
            this.latitude = pano.getLngLat()[1] ;
            this.longitude = pano.getLngLat()[0] ;
        }

        public String getId() {
            return id;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }
    }

}
