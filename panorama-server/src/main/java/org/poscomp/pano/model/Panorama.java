package org.poscomp.pano.model;

import org.poscomp.streetview4j.model.PanoramaDetails;
import org.springframework.data.annotation.Id;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by dnmilne on 13/07/15.
 */
@Document
public class Panorama {

    @Id
    private String id ;

    @GeoSpatialIndexed
    private double[] lnglat ;

    @Indexed
    private double elevation ;

    private double pitch ;
    private double yaw ;
    private double tilt ;

    private String description ;

    @Indexed
    private String copyright ;

    private Set<String> linkedPanoramaIds ;


    private Panorama() {

    }

    public Panorama(PanoramaDetails details) {

        id = details.getLocation().getPanoId() ;

        lnglat = new double[2] ;
        lnglat[0] = details.getLocation().getLng() ;
        lnglat[1] = details.getLocation().getLat();

        elevation = (details.getLocation().getElevationEGM96() + details.getLocation().getElevationWGS84()) /2  ;

        pitch = details.getProjection().getTiltPitchDeg();
        yaw = details.getProjection().getPanoYawDeg();
        tilt = details.getProjection().getTiltYawDeg();


        description = details.getLocation().getDescription() ;
        copyright = details.getData().getCopyright() ;

        linkedPanoramaIds = new HashSet<String>() ;

        if (details.getLinks() == null)
            return ;

        for (PanoramaDetails.Link link:details.getLinks())
            linkedPanoramaIds.add(link.getPanoId()) ;
    }

    public Panorama(Panorama panorama) {
        this.id = panorama.getId() ;

        this.lnglat = panorama.getLngLat() ;
        this.elevation = panorama.getElevation() ;

        this.pitch = panorama.getPitch() ;
        this.yaw = panorama.getYaw() ;
        this.tilt = panorama.getTilt();

        this.description = panorama.getDescription() ;
        this.copyright = panorama.getCopyright() ;

        this.linkedPanoramaIds = new HashSet<String>() ;
        this.linkedPanoramaIds.addAll(panorama.getLinkedPanoramaIds()) ;

    }

    public String getId() {
        return id;
    }

    public double[] getLngLat() { return lnglat ; }

    public double getElevation() {
        return elevation;
    }

    public double getYaw() {
        return yaw;
    }

    public double getPitch() {
        return pitch;
    }

    public String getDescription() {
        return description;
    }

    public double getTilt() {
        return tilt;
    }

    public String getCopyright() {
        return copyright;
    }

    public Set<String> getLinkedPanoramaIds() {
        return linkedPanoramaIds;
    }

}
