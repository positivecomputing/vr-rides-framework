package org.poscomp.pano.model;

import com.wordnik.swagger.annotations.ApiModel;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;

/**
 * Created by dnmilne on 14/07/15.
 */

public class ImageJob {

    public enum State {running, completed, failed} ;

    public static final int DEFAULT_ZOOM = 3 ;
    public static final String DEFAULT_ZOOM_STR = "3" ;

    @Id
    private ObjectId id ;

    @Indexed
    private String panoramaId ;

    @Indexed
    private Integer zoom ;

    @Indexed
    private Date startedAt ;

    @Indexed
    private Date finishedAt ;

    @Indexed
    private State state ;

    public ImageJob() {

    }

    public ImageJob(String panoramaId, Integer zoom) {
        this.panoramaId = panoramaId;

        this.zoom = zoom;

        this.startedAt = new Date() ;

        this.state = State.running ;
    }

    public ImageJob(ImageJob job) {

        this.id = job.getId() ;

        this.panoramaId = job.getPanoramaId() ;
        this.zoom = job.getZoom() ;

        this.startedAt = job.getStartedAt() ;
        this.finishedAt = job.getFinishedAt() ;

        this.state = job.getState() ;
    }

    public ObjectId getId() {
        return id;
    }

    public String getPanoramaId() {
        return panoramaId;
    }

    public int getZoom() {

        if (zoom == null)
            zoom = DEFAULT_ZOOM ;

        return zoom;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public Date getFinishedAt() {
        return finishedAt;
    }

    public State getState() {
        return state;
    }

    public void finish(boolean successful) {
        finishedAt = new Date() ;

        if (successful)
            state = State.completed ;
        else
            state = State.failed ;
    }
}
