package org.poscomp.pano.model;

/**
 * Created by dnmilne on 14/07/15.
 */
public enum Projection {

    mercator,
    front,
    back,
    left,
    right,
    top,
    bottom

}
