package org.poscomp.pano.views;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;
import org.poscomp.pano.model.ImageJob;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;

/**
 * Created by dnmilne on 14/07/15.
 */
@ApiModel(value="ImageJob", description="A job that retrieves and caches panorama imagery")
public class VImageJob  {

    @ApiModelProperty(value="A unique id for this job")
    private String id ;

    @ApiModelProperty(value="The id of the panorama this job fetches images for")
    private String panoramaId ;

    @ApiModelProperty(value="The zoom level of images to fetch, from `1` (tiny) to `5` (huge). Defaults to `" + ImageJob.DEFAULT_ZOOM_STR + "`")
    private Integer zoom ;

    @ApiModelProperty(value="The date this job was started")
    private Date startedAt ;

    @ApiModelProperty(value="The date this job finished")
    private Date finishedAt ;

    @ApiModelProperty(value="Whether the job is running, completed, or failed")
    private ImageJob.State state ;

    @ApiModelProperty(value="The proportion of the job currently completed")
    private Double progress ;

    public VImageJob() {

    }

    public VImageJob(ImageJob job, Double progress) {

        if (job.getId() != null)
            this.id = job.getId().toString() ;

        this.panoramaId = job.getPanoramaId() ;

        this.zoom = job.getZoom() ;

        this.startedAt = job.getStartedAt() ;
        this.finishedAt = job.getFinishedAt() ;

        this.state = job.getState() ;

        if (progress != null)
            this.progress = progress ;
        else {
            if (job.getState() == ImageJob.State.running)
                this.progress = 0.0 ;
            else
                this.progress = 1.0 ;
        }
    }

    public String getId() {
        return id;
    }

    public String getPanoramaId() {
        return panoramaId;
    }

    public Integer getZoom() {
        return zoom;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public Date getFinishedAt() {
        return finishedAt;
    }

    public ImageJob.State getState() {
        return state;
    }

    public Double getProgress() {
        return progress;
    }
}
