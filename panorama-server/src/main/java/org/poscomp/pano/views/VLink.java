package org.poscomp.pano.views;

import com.javadocmd.simplelatlng.LatLng;
import com.javadocmd.simplelatlng.LatLngTool;
import com.javadocmd.simplelatlng.util.LengthUnit;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.poscomp.pano.model.ImageJob;
import org.poscomp.pano.model.Panorama;

/**
 * Created by dnmilne on 13/07/15.
 */
@ApiModel(value="Link", description="A link to another panorama")
public class VLink {

    @ApiModelProperty(value="id of the target panorama", position = 1)
    private String panoramaId ;

    @ApiModelProperty(value="Latitude of the panorama location", position=2)
    private double latitude ;

    @ApiModelProperty(value="Longitude of the panorama location", position=3)
    private double longitude ;

    @ApiModelProperty(value="Angle (degrees from north) to travel from source to target panorama", position=4)
    private double bearing ;

    @ApiModelProperty(value="Shortest distance (in meters) to travel from source to target panorama", position=5)
    private double distance ;

    @ApiModelProperty(value="Availability of imagery for the target panorama", position=6)
    private ImageDetails imageDetails ;

    public VLink(VPanorama source, VPanorama target) {

        panoramaId = target.getId() ;
        latitude = target.getLatitude() ;
        longitude = target.getLongitude() ;

        LatLng sourcePoint = new LatLng(source.getLatitude(), source.getLongitude()) ;
        LatLng targetPoint = new LatLng(target.getLatitude(), target.getLongitude()) ;

        bearing = LatLngTool.initialBearing(sourcePoint, targetPoint) ;
        distance = LatLngTool.distance(sourcePoint, targetPoint, LengthUnit.METER) ;
    }

    public VLink withImageDetails(VImageJob job) {
        imageDetails = new ImageDetails(job) ;
        return this ;
    }


    public String getPanoramaId() {
        return panoramaId;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getBearing() {
        return bearing;
    }

    public double getDistance() {
        return distance;
    }

    public ImageDetails getImageDetails() {
        return imageDetails;
    }
}
