package org.poscomp.pano.views;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.poscomp.pano.model.ImageJob;
import org.poscomp.pano.model.Panorama;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dnmilne on 13/07/15.
 */
@ApiModel(value="Panorama", description="Details of a panorama location")
public class VPanorama  {

    @ApiModelProperty(value="id of the panorama")
    private String id ;

    @ApiModelProperty(value="latitude of panorama location")
    private double latitude;

    @ApiModelProperty(value="longitude of panorama location")
    private double longitude ;

    @ApiModelProperty(value="elevation (meters above sealevel) of panorama location")
    private double elevation ;

    @ApiModelProperty(value="degrees from level that camera was facing")
    private double pitch ;

    @ApiModelProperty(value="degrees from north that camera was facing")
    private double yaw ;

    @ApiModelProperty(value="a plain-text description of the location")
    private String description ;

    @ApiModelProperty(value="the owner of the panorama imagery")
    private String copyright ;

    @ApiModelProperty(value="a list of other panoramas accessable from this location")
    private List<VLink> links ;

    @ApiModelProperty(value="availability of imagery for the panorama")
    private ImageDetails imageDetails ;

    public VPanorama(Panorama p) {

        id = p.getId() ;

        latitude = p.getLngLat()[1] ;
        longitude = p.getLngLat()[0] ;
        elevation = p.getElevation() ;

        pitch = p.getPitch() ;
        yaw = p.getYaw() ;

        description = p.getDescription() ;
        copyright = p.getCopyright() ;

    }

    public VPanorama withImageDetails(VImageJob imageJob) {
        imageDetails = new ImageDetails(imageJob) ;
        return this ;
    }

    public VPanorama withLink(VPanorama link, VImageJob imageJob) {

        if (links == null)
            links = new ArrayList<VLink>() ;

        links.add(
                new VLink(this, link).withImageDetails(imageJob)
        );

        return this ;
    }

    public String getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getElevation() {
        return elevation;
    }

    public double getPitch() {
        return pitch;
    }

    public double getYaw() {
        return yaw;
    }

    public String getDescription() {
        return description;
    }

    public String getCopyright() {
        return copyright;
    }

    public List<VLink> getLinks() {
        return links;
    }

    public ImageDetails getImageDetails() {
        return imageDetails;
    }
}
