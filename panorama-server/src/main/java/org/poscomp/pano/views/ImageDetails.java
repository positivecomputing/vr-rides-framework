package org.poscomp.pano.views;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.poscomp.pano.model.ImageJob;

/**
 * Created by dnmilne on 15/07/15.
 */
@ApiModel(description="Availability of imagery at a panorama location")
public class ImageDetails {

    public enum State {
        missing,
        loading,
        ready,
        failed
    }

    @ApiModelProperty(value="State of imagery for this panorama location")
    private State state ;

    @ApiModelProperty(value="If **state** = `loading`, then this describes current progress between `0` and `1`")
    private double progress ;

    public ImageDetails(VImageJob job) {

        if (job == null) {
            state = State.missing ;
            return ;
        }

        switch (job.getState()) {
            case running:
                state = State.loading ;
                progress = job.getProgress() ;
                break ;
            case completed:
                state = State.ready ;
                progress = 1 ;
                break ;
            case failed:
                state = State.failed ;
        }
    }

    public State getState() {
        return state;
    }

    public double getProgress() {
        return progress;
    }
}
