package org.poscomp.pano.config;

import com.wordnik.swagger.models.Info;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpSession;

@Configuration
@EnableSwagger2
@PropertySource("classpath:swagger.properties")
public class SwaggerConfig {

    @Autowired
    private Environment env ;

    @Bean
    public Docket customImplementation(){

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo()) ;
    }

    @Bean
    public ApiInfo apiInfo() {

        return new ApiInfo(
                env.getProperty("api.title"),
                env.getProperty("api.description"),
                env.getProperty("api.version"),
                env.getProperty("api.tos.url"),
                env.getProperty("api.contact"),
                env.getProperty("api.licence"),
                env.getProperty("api.licence.url")
        ) ;
    }
}