package org.poscomp.pano.config;



import org.poscomp.pano.error.BadRequest;
import org.poscomp.pano.error.Forbidden;
import org.poscomp.pano.error.NotFound;
import org.poscomp.pano.error.Unauthorized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by dmilne on 10/06/2014.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(BadRequest.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody ApiError handle(BadRequest error){

        logger.warn("Bad request", error) ;
        return new ApiError(error) ;
    }

    @ExceptionHandler(Unauthorized.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public @ResponseBody ApiError handle(Unauthorized error){
        logger.warn("Unauthorized", error) ;
        return new ApiError(error) ;
    }


    @ExceptionHandler(Forbidden.class)
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public @ResponseBody ApiError handle(Forbidden error){
        logger.warn("Forbidden", error) ;
        return new ApiError(error) ;
    }



    @ExceptionHandler(NotFound.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public @ResponseBody ApiError handle(NotFound error){
        logger.warn("!Not found", error) ;
        return new ApiError(error) ;
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ApiError handle(Exception error){

        logger.warn("Internal server error", error) ;
        return new ApiError(error) ;
    }


    public static class ApiError {

        private String message ;

        public ApiError(Throwable e) {
            this.message = e.getMessage() ;
        }

        public String getMessage() {
            return message;
        }
    }

}
