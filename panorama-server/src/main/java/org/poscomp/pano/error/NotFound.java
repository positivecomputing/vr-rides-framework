package org.poscomp.pano.error;

/**
 * Created by dmilne on 9/09/2014.
 */
public class NotFound extends Exception {

    public NotFound() {
        super() ;
    }

    public NotFound(String message) {
        super(message) ;
    }

    public NotFound(String message, Throwable cause) {
        super(message, cause) ;
    }

}
