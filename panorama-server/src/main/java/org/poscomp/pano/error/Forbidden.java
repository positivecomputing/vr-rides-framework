package org.poscomp.pano.error;

/**
 * Created by dmilne on 8/10/2014.
 */
public class Forbidden extends Exception{

    public Forbidden() {
        super() ;
    }

    public Forbidden(String message) {
        super(message) ;
    }

    public Forbidden(String message, Throwable cause) {
        super(message, cause) ;
    }
}
