package org.poscomp.pano.error;

/**
 * Created by dmilne on 9/09/2014.
 */
public class BadRequest extends Exception {

    public BadRequest() {
        super() ;
    }

    public BadRequest(String message) {
        super(message) ;
    }

    public BadRequest(String message, Throwable cause) {
        super(message, cause) ;
    }
}
