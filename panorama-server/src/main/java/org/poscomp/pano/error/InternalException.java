package org.poscomp.pano.error;

/**
 * Created by dmilne on 9/09/2014.
 */
public class InternalException extends Exception {

    public InternalException() {
        super() ;
    }

    public InternalException(String message) {
        super(message) ;
    }

    public InternalException(String message, Throwable cause) {
        super(message, cause) ;
    }
}
