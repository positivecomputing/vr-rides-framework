package org.poscomp.pano.error;

/**
 * Created by dmilne on 9/09/2014.
 */
public class Unauthorized extends Exception {


    public Unauthorized() {
        super() ;
    }

    public Unauthorized(String message) {
        super(message) ;
    }

    public Unauthorized(String message, Throwable cause) {
        super(message, cause) ;
    }
}
