package org.poscomp.pano.util;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.poscomp.pano.model.CrawlJob;
import org.poscomp.pano.model.ImageJob;
import org.poscomp.pano.model.Panorama;
import org.poscomp.pano.model.Projection;
import org.poscomp.pano.repo.ImageJobRepository;
import org.poscomp.pano.repo.ImageRepository;
import org.poscomp.pano.repo.PanoramaRepository;
import org.poscomp.streetview4j.StreetviewClient;
import org.poscomp.streetview4j.model.PanoramaDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by dnmilne on 21/10/15.
 */
@Component
public class ImageCrawler implements InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(ImageCrawler.class) ;

    @Autowired
    private ImageRepository imageRepo ;

    @Autowired
    private PanoramaRepository panoRepo ;

    @Autowired
    private ImageJobRepository jobRepo ;

    @Autowired
    private ImageRetriever retriever ;

    private StreetviewClient streetview  ;

    private Worker currentWorker ;

    public boolean run(CrawlJob job) {

        if (currentWorker != null && currentWorker.isRunning()) {
            logger.warn("A crawl job is already running") ;
            return false ;
        }

        currentWorker = new Worker(job) ;
        currentWorker.start() ;
        return true ;
    }




    @Override
    public void afterPropertiesSet() throws Exception {

        streetview = new StreetviewClient() ;

        retriever.addListener(
                new ImageRetriever.Listener() {

                    @Override
                    public void jobAborted(ImageJob job) {

                        if (currentWorker == null) return;

                        currentWorker.handleImageJobAborted(job.getPanoramaId());
                    }

                    @Override
                    public void jobCompleted(ImageJob job) {

                        if (currentWorker == null) return;

                        currentWorker.handleImageJobCompleted(job.getPanoramaId());
                    }
                }
        );
    }






    public CrawlJob getCurrentJob() {

        if (currentWorker == null)
            return null ;

        return currentWorker.getJob();
    }

    private class Worker extends Thread {

        private CrawlJob job ;

        private boolean running = false ;

        public Worker(CrawlJob job) {
            this.job = job ;
        }


        public void run() {

            running = true ;

            doNextItem() ;

            while (job.getCompletedAt() == null) {

                try {
                    Thread.sleep(1000) ;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            running = false ;

        }


        public void handleImageJobCompleted(String panoramaId) {

            job.handleCompleted(panoramaId);
            doNextItem();
        }

        public void handleImageJobAborted(String panoramaId) {

            job.handleAborted(panoramaId);
            doNextItem();
        }



        private boolean doNextItem() {

            CrawlJob.Item item = job.poll() ;

            if (item == null) {
                //nothing to do
                return false;
            }

            try {
                //fetch panorama details
                PanoramaDetails details = streetview.getPanoramaDetails(item.getPanoramaId()) ;
                panoRepo.save(new Panorama(details)) ;

                if (item.getSteps() < job.getMaxSteps()) {

                    //queue up any links to also be processed
                    for (PanoramaDetails.Link link : details.getLinks()) {
                        try {
                            PanoramaDetails linkDetails = streetview.getPanoramaDetails(link.getPanoId());
                            job.offer(linkDetails, item.getSteps() + 1);
                        } catch (Exception e) {
                            logger.warn("Could not retrieve link panorama " + link.getPanoId(), e);
                        }
                    }
                }

            } catch (Exception e) {
                logger.warn("Could not retrieve panorama " + item.getPanoramaId(), e) ;
            }

            if (imageRepo.exists(item.getPanoramaId(), job.getZoom(), Projection.mercator)) {
                //we already have this imagery, so immediately treat this as completed
                handleImageJobCompleted(item.getPanoramaId());
                return true ;
            }

            retriever.startJob(item.getPanoramaId(), job.getZoom()) ;
            return true ;
        }

        public CrawlJob getJob() {
            return job;
        }

        public boolean isRunning() {
            return running;
        }
    }

}
