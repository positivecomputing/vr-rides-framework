package org.poscomp.pano.util;

import org.bson.types.ObjectId;
import org.poscomp.pano.model.ImageJob;
import org.poscomp.pano.model.Projection;
import org.poscomp.pano.repo.ImageJobRepository;
import org.poscomp.pano.repo.ImageRepository;
import org.poscomp.skybox.Side;
import org.poscomp.skybox.SkyboxExtractor;
import org.poscomp.streetview4j.StreetviewClient;
import org.poscomp.streetview4j.util.StreetviewJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by dnmilne on 15/07/15.
 */
@Component
public class ImageRetriever {

    private static final Logger logger = LoggerFactory.getLogger(ImageRetriever.class) ;

    @Autowired
    private ImageRepository imageRepo ;

    @Autowired
    private ImageJobRepository jobRepo ;

    private Map<ObjectId,Worker> workersByJobId = new HashMap<ObjectId, Worker>() ;

    private StreetviewClient streetview = new StreetviewClient();

    public List<Listener> listeners = new ArrayList<Listener>() ;

    public void addListener(Listener listener) {
        listeners.add(listener) ;
    }

    public ImageJob startJob(String panoramaId, int zoom) {

        ImageJob job = new ImageJob(panoramaId, zoom) ;
        jobRepo.save(job) ;

        Worker worker = new Worker(job) ;
        workersByJobId.put(job.getId(), worker) ;
        worker.start() ;

        return job ;
    }

    public Double getProgress(ImageJob job) {

        Worker worker = workersByJobId.get(job.getId()) ;

        if (worker == null)
            return null ;

        return worker.getJobProgress() ;
    }


    private class Worker extends Thread {

        private ImageJob job ;

        private double panoramaProgress = 0 ;

        private int sidesExtracted = 0 ;

        public Worker(ImageJob job) {
            this.job = job ;
        }


        public void run() {

            logger.info("Starting job " + job.getId() + " for pano " + job.getPanoramaId()) ;

            StreetviewJob j = streetview.getImageJob(job.getPanoramaId(), job.getZoom()) ;

            ExecutorService executor = Executors.newFixedThreadPool(1);
            Future<BufferedImage> future=executor.submit(j) ;

            while (!j.isDone()) {

                panoramaProgress = j.getProgress() ;

                try {
                    Thread.sleep(500) ;
                } catch (InterruptedException e) {
                    logger.debug("Insomnia", e) ;
                }
            }


            BufferedImage mainImage ;
            try {
                mainImage = future.get() ;
            } catch (Exception e) {
                logger.error("Could not download main image for " + job.getPanoramaId() + " z" + job.getZoom(), e) ;
                handleAbort() ;
                return ;
            }



            if (mainImage == null || mainImage.getWidth() < 10 || mainImage.getHeight() < 10) {
                logger.error("Downloaded empty main image for " + job.getPanoramaId() + " z" + job.getZoom()) ;
                handleAbort(); ;
                return ;
            }


            try {
                imageRepo.save(mainImage, job.getPanoramaId(), job.getZoom(), Projection.mercator);
            } catch (IOException e) {
                logger.error("Could not save main image for " + job.getPanoramaId() + " z" + job.getZoom(), e);
                handleAbort();
                return;
            }


            SkyboxExtractor skybox = new SkyboxExtractor(mainImage) ;

            for (Side side: Side.values()) {

                Projection projection = Projection.valueOf(side.name()) ;

                BufferedImage sideImage ;
                try{
                    sideImage = skybox.extractSide(side) ;
                } catch (Exception e) {
                    logger.error("Could not extract " + side + " image for " + job.getPanoramaId() + " z" + job.getZoom(), e) ;
                    handleAbort() ;
                    return ;
                }

                if (sideImage == null){
                    logger.error("Could not extract " + side + " image for " + job.getPanoramaId() + " z" + job.getZoom(), new NullPointerException()) ;
                    handleAbort() ;
                    return ;
                }


                try {
                    imageRepo.save(sideImage, job.getPanoramaId(), job.getZoom(), projection);
                } catch (IOException e) {
                    logger.error("Could not save " + side + " image for " + job.getPanoramaId() + " z" + job.getZoom(), e) ;
                    handleAbort() ;
                    return ;
                }

                sidesExtracted++ ;
            }

            handleCompletion() ;
        }

        private void handleAbort() {

            logger.warn("Aborted image job " + job.getId()) ;

            job.finish(false);
            jobRepo.save(job) ;

            for (Listener listener:listeners)
                listener.jobAborted(job);

            workersByJobId.remove(job.getId()) ;
        }

        private void handleCompletion() {
            logger.info("Completed image job " + job.getId()) ;

            job.finish(true);
            jobRepo.save(job) ;

            for (Listener listener:listeners)
                listener.jobCompleted(job);

            workersByJobId.remove(job.getId()) ;
        }

        public double getJobProgress() {

            double sideProgress = (double) sidesExtracted / 6 ;

            return (panoramaProgress * 0.75) + (sideProgress * 0.25) ;
        }
    }


    public interface Listener {

        public void jobAborted(ImageJob job) ;

        public void jobCompleted(ImageJob job) ;

    }


}
