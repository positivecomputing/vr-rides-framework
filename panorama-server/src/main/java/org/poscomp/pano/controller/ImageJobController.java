package org.poscomp.pano.controller;

import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.poscomp.pano.error.BadRequest;
import org.poscomp.pano.error.InternalException;
import org.poscomp.pano.error.NotFound;
import org.poscomp.pano.model.CrawlJob;
import org.poscomp.pano.model.ImageJob;
import org.poscomp.pano.model.Panorama;
import org.poscomp.pano.repo.ImageJobRepository;
import org.poscomp.pano.repo.ImageRepository;
import org.poscomp.pano.repo.PanoramaRepository;
import org.poscomp.pano.util.ImageCrawler;
import org.poscomp.pano.util.ImageRetriever;
import org.poscomp.pano.views.VImageJob;
import org.poscomp.streetview4j.StreetviewClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by dnmilne on 14/07/15.
 */
@Controller
@Api(value="Image Jobs", description = "Services for launching and monitoring image jobs, which retrieve and process imagery for individual panoramas")
public class ImageJobController {

    private static final Logger logger = LoggerFactory.getLogger(ImageJobController.class) ;

    @Autowired
    ImageJobRepository jobRepo ;

    @Autowired
    PanoramaRepository panoRepo ;

    @Autowired
    ImageRetriever imageRetriever ;

    @ApiOperation(
            value="Returns previously launched jobs, in descending order of when they were started.",
            notes="Returns previously launched jobs, in descending order of when they were started.\n\n" +
                    "This only returns the 20 most recently started jobs, so use **startedBefore** to page " +
                    "through other results."
    )
    @RequestMapping(value="/imagejobs", method= RequestMethod.GET)
    public @ResponseBody
    List<VImageJob> getJobs(

            @ApiParam(value = "A filter to restrict returned results to those started after the given date", required = false)
            @RequestParam(required = false)
            @DateTimeFormat(iso= DateTimeFormat.ISO.DATE_TIME)
            Date startedBefore,

            @ApiParam(value = "A filter to restrict returned results to those related to given panorama", required = false)
            @RequestParam(required = false)

            String panoramaId,

            @ApiParam(value = "A filter to restrict returned results to those in given state", required = false)
            @RequestParam(required = false)
            ImageJob.State state

    ) throws BadRequest, InternalException {

        List<ImageJob> jobs = jobRepo.find(startedBefore,panoramaId,state) ;

        List<VImageJob> hydratedJobs = new ArrayList<VImageJob>() ;

        for (ImageJob job:jobs)
            hydratedJobs.add(hydrate(job)) ;

        return hydratedJobs ;
    }


    @ApiOperation(
            value="Returns status of a single, previously launched image job"
    )
    @RequestMapping(value="/imagejobs/{jobId}", method= RequestMethod.GET)
    public @ResponseBody
    VImageJob getJob(

            @ApiParam(value = "Unique id of the image job to retrieve", required = true)
            @PathVariable
            String jobId

    ) throws NotFound, BadRequest, InternalException {

        if (!ObjectId.isValid(jobId))
            throw new BadRequest("Invalid job id") ;

        ImageJob job = jobRepo.findOne(new ObjectId(jobId)) ;

        if (job == null)
            throw new NotFound("Job does not exist") ;

        return hydrate(job) ;
    }

    @ApiOperation(
            value="Launches a new image fetching/processing job"
    )
    @RequestMapping(value="/imagejobs", method= RequestMethod.POST)
    public @ResponseBody VImageJob postJob(

            @ApiParam("Details of the image job to launch. This should only specify **panoramaId** and **zoom**; all other properties are ignored.")
            @RequestBody
            VImageJob job

    ) throws BadRequest, NotFound {

        String panoramaId = job.getPanoramaId() ;

        if (job.getId() != null)
            throw new BadRequest("Do not specify an id for new jobs") ;

        if (panoramaId == null)
            throw new BadRequest("Job must specify a panorama id") ;

        Panorama panorama = panoRepo.findOne(panoramaId) ;
        if (panorama == null)
            throw new NotFound("Panorama " + panoramaId + " does not exist, or hasn't been fetched yet") ;

        Integer zoom = job.getZoom() ;

        if (zoom == null)
            zoom = ImageJob.DEFAULT_ZOOM ;


        ImageJob existingJob = jobRepo.findLatest(panoramaId, zoom) ;

        if (existingJob != null && existingJob.getState()==ImageJob.State.running) {

            //if image retriever actually knows about this job, then return it rather than starting a new one.
            if (imageRetriever.getProgress(existingJob) != null)
                return hydrate(existingJob) ;
        }

        ImageJob newJob = imageRetriever.startJob(panoramaId, zoom) ;
        return hydrate(newJob) ;
    }






    private VImageJob hydrate(ImageJob job) {

        return new VImageJob(job, imageRetriever.getProgress(job));

    }

}
