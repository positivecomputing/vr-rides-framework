package org.poscomp.pano.controller;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.poscomp.pano.error.InternalException;
import org.poscomp.pano.error.NotFound;
import org.poscomp.pano.model.ImageJob;
import org.poscomp.pano.model.Panorama;
import org.poscomp.pano.model.Projection;
import org.poscomp.pano.repo.ImageRepository;
import org.poscomp.pano.views.VPanorama;
import org.poscomp.skybox.Side;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dnmilne on 13/07/15.
 */
@Api(value="Images", description = "provides panorama images")
@Controller
public class ImageController {

    @Autowired
    private ImageRepository imageRepo ;

    @RequestMapping(value="/panoramas/{panoramaId}/images/{projection}.jpg", method= RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    @ApiOperation(
            value = "Returns a single image of the given projection type, for the given panorama",
            notes = "Images are only available after an imagejob has been posted and completed"
    )
    public @ResponseBody
    byte[] getPanorama(

            @PathVariable
            @ApiParam(value = "id of the panorama", required = true)
            String panoramaId,

            @PathVariable
            @ApiParam(value = "type of image", required = true)
            Projection projection,

            @ApiParam(value = "a zoom level between `1` (tiny) and `5` (huge)", required = false, defaultValue = ImageJob.DEFAULT_ZOOM_STR)
            @RequestParam(required = false, defaultValue = ImageJob.DEFAULT_ZOOM_STR)
            Integer zoom

    ) throws NotFound, InternalException {

        if (!imageRepo.exists(panoramaId, zoom, projection))
            throw new NotFound("panorama does not exist, or images have not been fetched yet") ;

        try {
            byte[] bytes = imageRepo.getAsBytes(panoramaId, zoom, projection) ;
            return bytes ;
        } catch (IOException e) {
            throw new InternalException("Could not fetch image " + panoramaId + " z=" + zoom + " p=" + projection, e) ;
        }

    }
}
