package org.poscomp.pano.controller;

import com.wordnik.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Created by dnmilne on 23/07/2015.
 */
@Api(hidden = true)
@Controller
public class WelcomeController {

    @ApiIgnore
    @RequestMapping(value = "/", method= RequestMethod.GET)
    public String home() {
        return "index.html";
    }
}
