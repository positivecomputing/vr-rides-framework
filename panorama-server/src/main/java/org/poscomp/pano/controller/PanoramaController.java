package org.poscomp.pano.controller;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.poscomp.pano.error.InternalException;
import org.poscomp.pano.error.NotFound;
import org.poscomp.pano.model.ImageJob;
import org.poscomp.pano.model.Panorama;
import org.poscomp.pano.model.Projection;
import org.poscomp.pano.repo.ImageJobRepository;
import org.poscomp.pano.repo.ImageRepository;
import org.poscomp.pano.repo.PanoramaRepository;
import org.poscomp.pano.util.ImageRetriever;
import org.poscomp.pano.views.VImageJob;
import org.poscomp.pano.views.VPanorama;
import org.poscomp.streetview4j.StreetviewClient;
import org.poscomp.streetview4j.model.PanoramaDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dnmilne on 13/07/15.
 */
@Api(value="Panoramas", description = "provides panorama details")
@Controller
public class PanoramaController implements InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(PanoramaController.class) ;

    @Autowired
    private PanoramaRepository panoRepo ;

    @Autowired
    private ImageRepository imageRepo ;

    @Autowired
    private ImageJobRepository jobRepo ;

    @Autowired
    private ImageRetriever imageRetriever ;

    private StreetviewClient streetview  ;

    @ApiOperation(
            value = "Returns details of a single panorama"
    )
    @RequestMapping(value="/panoramas/{panoramaId}", method= RequestMethod.GET)
    public @ResponseBody
    VPanorama getPanorama(

            @PathVariable
            @ApiParam(value = "id of the panorama", required = true )
            String panoramaId,

            @RequestParam(required = false, defaultValue="false")
            @ApiParam(value = "`true` if cached details should be discarded and re-retrieved from google", defaultValue = "false")
            Boolean refresh,

            @RequestParam(required = false, defaultValue = ImageJob.DEFAULT_ZOOM_STR)
            @ApiParam(value = "The zoom level considered when describing availability of imagery", defaultValue = ImageJob.DEFAULT_ZOOM_STR)
            Integer zoom

    ) throws NotFound, InternalException {

        Panorama pano = panoRepo.findOne(panoramaId) ;

        if (pano == null || refresh)
            pano = fetchAndSavePanorama(panoramaId) ;

        if (pano == null)
            throw new NotFound("Panorama " + panoramaId + " does not seem to exist") ;

        List<Panorama> linkedPanos = new ArrayList<Panorama>() ;

        for (String linkedId: pano.getLinkedPanoramaIds()) {
            Panorama linkedPano = panoRepo.findOne(linkedId) ;

            if (linkedPano == null) {
                try {
                    linkedPano = fetchAndSavePanorama(linkedId) ;

                } catch (InternalException e) {

                }
            }

            if (linkedPano != null)
                linkedPanos.add(linkedPano) ;
        }

        return hydrate(pano, zoom, linkedPanos) ;
    }

    private Panorama fetchAndSavePanorama(String panoramaId) throws InternalException {
        try {
            PanoramaDetails details = streetview.getPanoramaDetails(panoramaId) ;
            Panorama pano = new Panorama(details) ;
            panoRepo.save(pano) ;

            return pano ;
        } catch (Exception e) {
            throw new InternalException("Could not retrieve panorama details from google", e) ;
        }
    }

    private VPanorama hydrate(Panorama pano, int zoom, List<Panorama> links) {

        VPanorama vPano = new VPanorama(pano)
                .withImageDetails(getLatestImageJob(pano.getId(), zoom)) ;

        for (Panorama link: links) {
            vPano.withLink(new VPanorama(link), getLatestImageJob(link.getId(), zoom)) ;
        }

        return vPano ;
    }


    public VImageJob getLatestImageJob(String panoId, int zoom) {

        if (imageRepo.exists(panoId, zoom, Projection.mercator)) {
            ImageJob finishedJob = new ImageJob(panoId, zoom) ;
            finishedJob.finish(true);
            return hydrate(finishedJob) ;
        }

        ImageJob latestJob = jobRepo.findLatest(panoId, zoom) ;

        if (latestJob == null)
            return null ;

        if (latestJob.getState() == ImageJob.State.running) {
            Double progress = imageRetriever.getProgress(latestJob);

            if (progress == null) {
                logger.warn("Found running image job " + latestJob.getId() + " that retriever doesn't know about");
                return null;
            }
        }

        return hydrate(latestJob) ;
    }

    private VImageJob hydrate(ImageJob job) {
        return new VImageJob(job, imageRetriever.getProgress(job));
    }

    public void afterPropertiesSet() throws Exception {
        streetview  = new StreetviewClient() ;
    }
}
