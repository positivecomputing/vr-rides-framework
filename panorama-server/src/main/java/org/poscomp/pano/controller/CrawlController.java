package org.poscomp.pano.controller;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.poscomp.pano.error.BadRequest;
import org.poscomp.pano.error.InternalException;
import org.poscomp.pano.error.NotFound;
import org.poscomp.pano.model.CrawlJob;
import org.poscomp.pano.model.Panorama;
import org.poscomp.pano.repo.ImageJobRepository;
import org.poscomp.pano.repo.PanoramaRepository;
import org.poscomp.pano.util.ImageCrawler;
import org.poscomp.pano.util.ImageRetriever;
import org.poscomp.streetview4j.StreetviewClient;
import org.poscomp.streetview4j.model.PanoramaDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * Created by dnmilne on 31/03/16.
 */
@Controller
@Api(value="Image Crawls", description = "Services for launching and monitoring image crawls, which retrieve and process imagery for large numbers of panoramas")
public class CrawlController implements InitializingBean{

    private static final Logger logger = LoggerFactory.getLogger(ImageJobController.class) ;

    @Autowired
    ImageCrawler imageCrawler ;

    StreetviewClient streetview ;

    @ApiOperation(
            value="Returns status of the currently running crawl"
    )
    @RequestMapping(value="/crawls/current", method= RequestMethod.GET)
    public @ResponseBody
    CrawlJob getJob(

    ) throws BadRequest, InternalException {

        CrawlJob currentJob = imageCrawler.getCurrentJob() ;

        return currentJob ;


    }

    @ApiOperation(
            value="Starts a new crawl"
    )
    @RequestMapping(value="/crawls", method= RequestMethod.POST)
    public @ResponseBody
    CrawlJob postJob(

            @ApiParam("Details of the crawl job to launch. This should only specify **seed**, **zoom** and **maxSteps**; all other properties are ignored.")
            @RequestBody
            CrawlJob job


    ) throws NotFound, BadRequest, InternalException {

        CrawlJob currentJob = imageCrawler.getCurrentJob() ;

        if (currentJob != null && currentJob.getCompletedAt() == null)
            throw new InternalException("A job is already running") ;

        PanoramaDetails pano ;
        try {
            pano = streetview.getPanoramaDetails(job.getSeed().getId()) ;
        } catch (Exception e) {
            throw new InternalException("Could not retrieve initial panorama", e) ;
        }

        CrawlJob newJob = new CrawlJob(new Panorama(pano), job.getZoom(), job.getMaxSteps()) ;

        imageCrawler.run(newJob);

        return job ;
    }


    public void afterPropertiesSet() throws Exception {
        streetview  = new StreetviewClient() ;
    }

}
