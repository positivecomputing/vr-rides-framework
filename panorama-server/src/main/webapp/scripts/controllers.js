angular.module("panorama-server.controllers", ['ng-swagger','ui.bootstrap', 'restangular','ngMap'])


    .controller('PanoramasCtrl', function($scope, $timeout, $uibModal,NgMap, Restangular) {

        $scope.query ;
        $scope.zoom = 3 ;

        $scope.markers = {} ;

        $scope.search = function() {

            var panoId = getPanoramaId($scope.query) ;
            loadPanorama(panoId) ;
        };

        function loadPanorama(id) {

            clear() ;
            $scope.query = id ;
            $scope.panoramaState = 'loading' ;

            Restangular.one("panoramas",id).get({zoom:$scope.zoom})
                .then(
                function (data) {
                    $scope.panoramaState = 'found' ;
                    $scope.panorama = data ;
                    handlePanoramaUpdated() ;
                },
                function (error) {

                    if (error.code==404)
                        $scope.panoramaState = 'missing' ;
                    else {
                        $scope.panoramaState = 'error' ;
                        console.log(error) ;
                    }
                }
            );

        }

        $scope.$watch("zoom", function() {

            if ($scope.panorama)
                loadPanorama($scope.panorama.id) ;

        }, true) ;


        function getPanoramaId(query) {

            var urlPattern = new RegExp("\!1s([^!]+)\!");

            var match = urlPattern.exec(query) ;

            if (match)
                return match[1] ;

            //assume query is a panorama id.
            return query ;
        }

        function handlePanoramaUpdated() {

            NgMap.getMap({id:"panoramaMap"}).then(function(map) {


                $timeout(function() {

                    google.maps.event.trigger(map, 'resize');
                    map.setCenter({lat: $scope.panorama.latitude, lng: $scope.panorama.longitude}) ;

                    //remove past markers
                    _.each($scope.markers, function(marker) {
                        marker.setMap(null) ;
                        google.maps.event.clearListeners(marker, "click") ;
                    }) ;

                    $scope.markers = {} ;

                    $scope.markers[$scope.panorama.id] = new google.maps.Marker({
                        position: {lat:$scope.panorama.latitude, lng:$scope.panorama.longitude},
                        map: map,
                        panoramaId: $scope.panorama.id,
                        imageDetails: $scope.panorama.imageDetails,
                        icon: {
                            url:getImageIcon($scope.panorama.imageDetails),
                            scaledSize: new google.maps.Size(40, 40)
                        }
                    });

                    _.each($scope.panorama.links, function(link) {

                        var linkMarker = new google.maps.Marker({
                            position: {lat:link.latitude, lng:link.longitude},
                            map: map,
                            panoramaId: link.panoramaId,
                            imageDetails: link.imageDetails,
                            icon:  {
                                url: getImageIcon(link.imageDetails),
                                scaledSize: new google.maps.Size(20, 20)
                            },
                        });

                        $scope.markers[link.panoramaId] = linkMarker ;

                        google.maps.event.addListener(linkMarker, "click", function() {
                            console.log("BLAH!") ; loadPanorama(link.panoramaId)
                        }) ;
                    }) ;

                    console.log($scope.markers) ;
                },0) ;
            }) ;

        }


        $scope.cacheImages = function() {

            console.log("caching images") ;

            var job = {
                panoramaId:$scope.panorama.id,
                zoom:$scope.zoom
            } ;

            console.log(job) ;

            Restangular.all("imagejobs").post(job).then(
                function (data) {
                    $scope.imageJob = data ;
                    updateImageJobStatus() ;
                },
                function (error) {
                    console.log(error) ;
                }
            ) ;
        } ;

        $scope.showImages = function() {

            var modalInstance = $uibModal.open({
                templateUrl: 'views/modals/viewImages.html',
                controller: 'ImageDlgCtrl',
                resolve: {
                    panorama: function () {
                        return $scope.panorama;
                    },
                    zoom: function() {
                        return $scope.zoom ;
                    }
                }
            });

            modalInstance.result.then() ;

        } ;

        function updateImageJobStatus() {

            if (!$scope.imageJob)
                return ;

            if ($scope.imageJobPromise = null)
                $timeout.cancel($scope.imageJobPromise) ;

            Restangular.one("imagejobs", $scope.imageJob.id).get()
            .then(
                function (data) {
                    $scope.imageJob = data ;

                    var marker = $scope.markers[$scope.imageJob.panoramaId] ;
                    if (marker) {

                        marker.icon.url = getImageIconFromJobStatus($scope.imageJob) ;

                        NgMap.getMap({id:"panoramaMap"}).then(function(map) {
                            marker.setMap(null) ;
                            marker.setMap(map) ;
                        }) ;
                    }

                    if ($scope.imageJob.state == 'running')
                        $scope.imageJobPromise = $timeout(updateImageJobStatus, 500);
                    else
                        $scope.search($scope.panorama.id) ;

                },
                function (error) {
                    console.log(error) ;
                }
            ) ;
        }

        function clear() {
            $scope.panorama = null ;
            $scope.panoramaState = null;
            $scope.imageJob = null ;

            if ($scope.imageJobPromise) {
                $timeout.cancel($scope.imageJobPromise);
                $scope.imageJobPromise = null ;
            }
        }

        function getImageIcon(imageDetails) {

            switch(imageDetails.state) {
                case 'missing' : return "img/camera-muted.svg" ;
                case 'ready' : return "img/camera-success.svg" ;
                case 'failed' : return "img/camera-danger.svg" ;
                case 'loading' : return "img/camera-warning.svg" ;
            }
        }

        function getImageIconFromJobStatus(imageJob) {

            switch(imageJob.state) {
                case 'completed' : return "img/camera-success.svg" ;
                case 'failed' : return "img/camera-danger.svg" ;
                case 'running' : return "img/camera-warning.svg" ;
            }
        }


    })



    .controller('CrawlsCtrl', function($scope, $timeout, $uibModal, NgMap, Restangular) {

        $scope.crawlState = 'missing' ;
        $scope.crawl = null ;

        Restangular.one("crawls", "current").get()
            .then(
            function (data) {

                if (!data) {
                    $scope.crawlState = 'missing';
                } else {

                    $scope.crawlState = 'found';
                    $scope.crawl = data ;

                    handleCrawlRetrieved() ;
                }
            },
            function (error) {

                if (error.code==404)
                    $scope.crawlState = 'missing' ;
                else {
                    $scope.crawlState = 'error' ;
                    console.log(error) ;
                }
            }
        );


        function handleCrawlRetrieved() {

            NgMap.getMap({id:'crawlMap'}).then(function(map) {
                $timeout(function() {
                    google.maps.event.trigger(map, 'resize');
                    map.setCenter({lat: $scope.crawl.seed.latitude, lng: $scope.crawl.seed.longitude}) ;
                },0) ;
            }) ;

            if (!$scope.crawl.completedAt)
                $timeout(updateCrawlStatus, 500);
        }


        function updateCrawlStatus() {

            Restangular.one("crawls", "current").get()
                .then(
                function (data) {
                    $scope.crawl = data ;

                    if (!$scope.crawl.completedAt)
                        $timeout(updateCrawlStatus, 500);
                },
                function (error) {
                    console.log(error) ;
                }
            ) ;
        }



        $scope.startCrawl = function() {


            var modalInstance = $uibModal.open({
                templateUrl: 'views/modals/startCrawl.html',
                controller: 'CrawlDlgCtrl',
                size:'sm'
            });

            modalInstance.result.then(

                function (crawl) {

                    $scope.crawl = null ;
                    $scope.crawlState = 'loading' ;

                    Restangular.all("crawls").post(crawl)
                        .then(
                        function(data) {
                            console.log(data) ;

                            $scope.crawl = data ;
                            $scope.crawlState = 'found';


                            handleCrawlRetrieved() ;
                        },
                        function(error) {
                            console.log(error) ;

                            $scope.crawlState = 'error' ;
                        }
                    ) ;
                }
            );
        } ;

        $scope.size = function(collection) {
            return _.size(collection);
        }


    })

    .controller('ApiCtrl', function($scope) {
        $scope.url="/v2/api-docs" ;
    })




    .controller('CrawlDlgCtrl', function($scope, $timeout, $uibModalInstance, NgMap, Restangular) {

        $scope.query ;
        $scope.zoom = 3 ;
        $scope.maxSteps = 20 ;

        $scope.search = function() {

            var panoId = getPanoramaId($scope.query) ;

            $scope.panorama = null ;
            $scope.panoramaState = 'loading' ;

            Restangular.one("panoramas", panoId).get()
                .then(
                function (data) {
                    $scope.panoramaState = 'found' ;
                    $scope.panorama = data ;
                    handlePanoramaUpdated() ;
                },
                function (error) {

                    if (error.code==404)
                        $scope.panoramaState = 'missing' ;
                    else {
                        $scope.panoramaState = 'error' ;
                        console.log(error) ;
                    }
                }
            );
        };

        function getPanoramaId(query) {

            var urlPattern = new RegExp("\!1s([^!]+)\!");

            var match = urlPattern.exec(query) ;

            if (match)
                return match[1] ;

            //assume query is a panorama id.
            return query ;
        }


        function handlePanoramaUpdated() {

            NgMap.getMap({id:'newCrawlMap'}).then(function(map) {
                $timeout(function() {
                    google.maps.event.trigger(map, 'resize');
                    map.setCenter({lat: $scope.panorama.latitude, lng: $scope.panorama.longitude}) ;
                },0) ;

            }) ;
        }

        $scope.startCrawl = function() {

            $uibModalInstance.close({
                seed: {
                    id:$scope.panorama.id,
                    latitude:$scope.panorama.latitude,
                    longitude:$scope.panorama.longitude,
                },
                zoom:$scope.zoom,
                maxSteps:$scope.maxSteps
            });
        } ;



        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        }

    })



    .controller('ImageDlgCtrl', function($scope, $uibModalInstance, panorama, zoom) {

        $scope.panorama = panorama ;
        $scope.zoom = zoom ;


        $scope.imageTypes = [
            {id:'mercator', name:"Wrapped image"},
            {id:'front', name:"Front projection"},
            {id:'back', name:"Back projection"},
            {id:'left', name:"Left projection"},
            {id:'right', name:"Right projection"},
            {id:'top', name:"Top projection"},
            {id:'bottom', name:"Bottom projection"}
        ] ;

        $scope.imageType = $scope.imageTypes[0] ;

        $scope.setImageType = function(imageType) {

            $scope.imageType = imageType ;
            $scope.imageUrl = "panoramas/" + $scope.panorama.id
                + "/images/" + $scope.imageType.id
                + ".jpg?zoom=" + $scope.zoom ;
        } ;

        $scope.close = function() {
            $uibModalInstance.dismiss('close');
        } ;

        $scope.setImageType($scope.imageTypes[0]) ;

    }) ;



