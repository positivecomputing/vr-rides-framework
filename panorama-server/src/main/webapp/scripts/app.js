var app = angular.module('panorama-server', ['panorama-server.controllers','ngRoute'])


    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'views/pages/panoramas.html',
                controller: "PanoramasCtrl"
            }).
            when('/crawls', {
                templateUrl: 'views/pages/crawls.html',
                controller: "CrawlsCtrl"
            }).
            when('/api', {
                templateUrl: 'views/pages/api.html',
                controller: "ApiCtrl"
            }).
            otherwise({redirectTo: '/'});
    }])

