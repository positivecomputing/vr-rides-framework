import com.javadocmd.simplelatlng.LatLng;
import org.junit.Before;
import org.junit.Test;
import org.poscomp.skybox.Side;
import org.poscomp.skybox.SkyboxExtractor;
import org.poscomp.streetview4j.StreetviewClient;
import org.poscomp.streetview4j.util.StreetviewJob;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by dnmilne on 9/07/15.
 */
public class PanoramaRetrieval {

    StreetviewClient streetview ;

    @Before
    public void setup() {
        streetview = new StreetviewClient() ;
    }

    @Test
    public void testPanoramaRetrieval() throws Exception {

        String id = "f_695QQ9L5oAAAQqZe0GgA" ;
        int zoom = 4 ;

        BufferedImage image = streetview.getImage(id, zoom) ;

        File dir = new File("data/" + id + "/" + zoom) ;
        dir.mkdirs() ;
        dir.mkdir() ;

        ImageIO.write(image, "jpg", new File(dir.getPath() + "/main.jpg")) ;

        SkyboxExtractor skybox = new SkyboxExtractor(image) ;

        for (Side side: Side.values()) {
            BufferedImage sideImage = skybox.extractSide(side) ;
            ImageIO.write(sideImage, "jpg", new File(dir.getPath() + "/" + side + ".jpg")) ;
        }

    }

    @Test
    public void testAsyncronousRetrieval() throws Exception {

        String id = "XiHSydJRwU---sAAAQfCadA4g" ;
        int zoom = 4 ;

        StreetviewJob job = streetview.getImageJob(id, zoom) ;

        ExecutorService executor = Executors.newFixedThreadPool(1);
        Future<BufferedImage> future=executor.submit(job) ;

        while (!job.isDone()) {
            System.out.println(job.getProgress()) ;
            Thread.sleep(500) ;
        }

        BufferedImage panorama = future.get() ;

        System.out.println(panorama.getWidth() + ":" + panorama.getHeight()) ;

    }

}
